import React, { Fragment, useState, useEffect } from "react";
import Header from "./components/Header";
import Formulario from "./components/Formulario";
import Clima from "./components/Clima";
import Error from "./components/Error";

function App() {
  // State del formulario
  const [datos, setDatos] = useState({
    ciudad: "",
    pais: "",
  });

  const [consultar, setConsultar] = useState(false);
  const [resultado, setResultado] = useState({});
  const [error, setError] = useState(false);

  const { ciudad, pais } = datos;

  useEffect(() => {
    const consultarAPI = async () => {
      if (consultar) {
        const appId = "bfb82048dc044c06312a02579f76dbdb";
        const url = `http://api.openweathermap.org/data/2.5/weather?q=${ciudad},${pais}&appid=${appId}`;
        const respuesta = await fetch(url);
        const resultado = await respuesta.json();
        setResultado(resultado);
        setConsultar(false);

        // Detectar el codigo de respuesta
        if (resultado.cod === "404") setError(true);
        else setError(false);
      }
    };
    consultarAPI();
  }, [consultar]);

  let componente;
  if (error) componente = <Error mensaje="No hay resultados" />;
  else componente = <Clima resultado={resultado} />;

  return (
    <Fragment>
      <Header titulo="Clima React App" />
      <div className="contenedor-form">
        <div className="container">
          <div className="row">
            <div className="col m6 s12">
              <Formulario
                setDatos={setDatos}
                datos={datos}
                setConsultar={setConsultar}
              />
            </div>
            <div className="col m6 s12">{componente}</div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default App;
